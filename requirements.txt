celery
Django>=2.2,<2.3
django-crispy-forms
django-debug-toolbar
redis
requests
django-redis
six
django-anymail # Used for mandrill integration. See installation instruction at https://github.com/anymail/
psycopg2-binary>=2.8,<2.9
django-hijack>=2.1.10
django-hijack-admin>=2.1.7

# private requirements
nixa-emails>=2.0
nixa-users>=3.0
nixa-login
