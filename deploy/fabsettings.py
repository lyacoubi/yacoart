from uuid import uuid4

PROJECT_NAME = 'yacoart-project'
# deployment logs
PROJECT_CODE = '' # todo
DEPLOYMENT_LOG_BASE_URL = 'http://nixa.ca/'
# ssh link to repository
REPOSITORY = 'git@bitbucket.org:nixateam/yacoart-project.git'  # todo
DOCKER_COMPOSE_VERSION = '1.14.0'
WEB_SERVICE = 'web'
DOCKER_GC_CONTENT = """
#!/bin/bash
docker container prune -f
docker image prune -a -f
rm -rf /var/lib/docker/aufs/diff/*-removing"""

STAGES = {
    'staging': {
        'hosts': ['24.37.82.222'],
        'default_branch': 'develop',
        'port': 'XXXX',
        'user': 'deploy',
        'docker_compose_file': 'docker-compose-staging.yml',
        'authorized_keys_file': 'authorized_keys'
    },
    'production': {
        'hosts': ['XXX.XXX.XXX.XXX'],
        'default_branch': 'master',
        'user': 'deploy',
        'docker_compose_file': 'docker-compose-prod.yml',
        'authorized_keys_file': 'authorized_keys_prod',
    }
}

ROOT_USER = 'root'


def random_password(): return str(uuid4()).replace('-', '')
